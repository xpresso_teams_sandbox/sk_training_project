import os
import shutil
import uuid
import tqdm
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.automl import utility
from xpresso.ai.core.commons.utils.constants import REPORT_OUTPUT_PATH, \
    EMPTY_STRING
from xpresso.ai.core.commons.utils.constants import LINE_SPACE
from xpresso.ai.core.data.visualization.report import Report


class MultivariateReport(Report):
    def __init__(self, dataset):
        super().__init__(dataset)

    def create_report(self, input_path=utils.DEFAULT_IMAGE_PATH,
                      output_path=REPORT_OUTPUT_PATH, file_name=None):
        """
        Function to create the report for the metrics of all the attributes
        Args:
            input_path (str): path to the plots to be fetched into the report
            output_path (str): path to the folder the report to be saved
            file_name(str): file name of report file
        """
        if file_name is None or file_name == EMPTY_STRING:
            file_name = "report_multivariate_{}.pdf".format(
                str(uuid.uuid4()))
        output_path, output_file_name = utility.get_file_from_path_string(
            output_path,
            file_name)
        if output_path is None:
            output_path = REPORT_OUTPUT_PATH
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        output_file_name = utility.set_extension(output_file_name, ".pdf")
        self.title_page()
        self.overview_page()
        self.add_multivariate_plots(input_path=input_path)
        self.output(os.path.join(output_path, output_file_name), 'F')

        if os.path.exists(input_path):
            shutil.rmtree(input_path)

    def add_multivariate_plots(self, input_path=utils.DEFAULT_IMAGE_PATH):
        """
        Helper function to add univariate distribution
        Args:
            input_path (str): path to the plots to be fetched into the report
        """
        print("\nPerforming Multivariate Report Generation\n")
        if not os.path.exists(input_path):
            print("No multivariate plots present for report generation")
            return
        correlation = ["spearman", "pearson", "chi_square"]
        files = os.listdir(input_path)
        for file_name in tqdm.tqdm(files):
            temp_file_name = file_name.split("_")
            temp_file_name = "_".join(temp_file_name[:-3])
            if temp_file_name in correlation:
                self.add_page()
                self.set_font('Times', 'B', 16)
                # Parameter specification: 0-width(full width), 20-height,
                # text within the cell, 0-No border, 1-cursor on newline,
                # 'C'-center aligned text
                self.cell(0, 20, "Correlation heatmap ({} coefficient)".format(
                    temp_file_name), 0, 1, 'C')
                self.ln(LINE_SPACE)  # 10 Line space
                # Logo (8, 70) coordinates of left top corner of image with
                # width=180mm
                self.image(os.path.join(input_path, file_name), 8, 70, 180)
