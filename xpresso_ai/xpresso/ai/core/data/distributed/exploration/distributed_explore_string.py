"""Exploration on String attribute column"""
__all__ = ["DistributedExploreString"]
__author__ = ["Sanyog Vyawahare"]


class DistributedExploreString():
    """Class for string analysis"""
    def __init__(self, data):
        self.data = data

    def populate_string(self):
        """
        Sets the metrics for string analysis
        :return: resp: Result of string analysis
        """
        resp = dict()
        freq_count = self.string_analysis(self.data)
        resp["freq_count"] = freq_count
        resp["tobedone"] = "Exploration of string type data"
        return resp

    @staticmethod
    def string_analysis(data):
        """
        Performs string analysis on attribute
        :param data: Attribute of type string
        :return: freq_count
        """
        freq_count = data.dropna().value_counts()
        freq_count = freq_count.to_dict()

        return freq_count
