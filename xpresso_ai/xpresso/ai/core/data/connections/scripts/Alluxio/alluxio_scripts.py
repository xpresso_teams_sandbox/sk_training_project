"""

    Class design for running scripts to deploy alluxio server for standalone machine

"""

__author__ = 'Shlok Chaudhari'
__all__ = 'dataconnector'

import subprocess
import os
import editor
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.data.connections.scripts.Alluxio.alluxio_config import alluxio_properties


class AlluxioServerAutomation:
    """

    Automating Alluxio server setup for a standalone machine

    """

    def __init__(self):
        """

        __init__() function here initializes the xpresso logger module
        """

        self.logger = XprLogger()

    def server_setup(self):
        """

        Scripts to setup alluxio server with single master.

        :return: None
        """

        os.chdir("/opt")
        subprocess.run(args=['wget', 'https://downloads.alluxio.io/downloads/'
                                     'files/2.0.0/alluxio-2.0.0-bin.tar.gz'],
                       stdout=subprocess.PIPE, text=True)
        subprocess.run(args=['tar', '-zxf',
                             'alluxio-2.0.0-bin.tar.gz'], stdout=subprocess.PIPE, text=True)
        os.chdir("/opt/alluxio-2.0.0")
        self.server_properties_setup()
        self.logger.info("Server Setup Complete")

    def server_properties_setup(self):
        """

        Script to setup supported file system properties.

        :return: None
        """

        os.chdir("/opt/alluxio-2.0.0/conf")
        temp_properties = subprocess.run(args=['cat', 'alluxio-site.properties.template'],
                                         stdout=subprocess.PIPE, text=True).stdout
        properties = temp_properties + alluxio_properties
        properties = bytes(properties, 'ascii')
        editor.edit(filename="alluxio-site.properties", contents=properties)
        self.logger.info(subprocess.run(args=['cat', 'alluxio-site.properties'],
                                        stdout=subprocess.PIPE, text=True).stdout)
        subprocess.call(args=['ssh-keygen'], shell=True)
        pub_key = subprocess.run(args=['cat', '/root/.ssh/id_rsa.pub'],
                                 stdout=subprocess.PIPE, text=True).stdout
        editor.edit(filename="/root/.ssh/authorized_keys", contents=bytes(pub_key, 'ascii'))
        os.chdir("/opt/alluxio-2.0.0")

    def start_server(self):
        """

        Script to start Alluxio server if user hasn't mounted the ramdisk
        or wants to remount it.

        :return: None
        """

        subprocess.call(args=['./bin/alluxio-start.sh all SudoMount'], shell=True)
        self.logger.info("Server started")

    def format_fs(self):
        """

        Formatting previously stored data and metadata, without affecting the under storage.
        NOTE: This step is only required when you run Alluxio for the first time.

        :return: None
        """

        subprocess.call(args=['./bin/alluxio format'], shell=True)
        self.logger.info("Format FS")

    def verify_sample_program(self):
        """

        Scripts to run a sample program.

        :return: None
        """

        subprocess.call(args=['./bin/alluxio runTests'], shell=True)
        self.logger.info("Verify sample program")


if __name__ == '__main__':
    auto_server = AlluxioServerAutomation()
    auto_server.server_setup()
    auto_server.format_fs()
    auto_server.start_server()
    auto_server.verify_sample_program()
