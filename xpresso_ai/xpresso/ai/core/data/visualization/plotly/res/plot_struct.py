# Developed By Nalin Ahuja, nalin.ahuja@abzooba.com

import xpresso.ai.core.data.visualization.plotly.res.utils as utils
import plotly.graph_objs as go
from plotly.offline import plot, iplot
from plotly.offline import init_notebook_mode
import os


class std_format(object):
    def apply_default_style(self):
        # File name
        self.filename = None

        # Plot Dimensions and Scale
        self.plot_width = None
        self.plot_height = None

        # Plot and Font Colors
        self.bg_color = "#fff"
        self.plot_color = "#fff"
        self.font_size = 18
        self.font_color = "#515151"

        # Plot Titles
        self.plot_title = None
        self.x_axis_title = None
        self.y_axis_title = None

        # Plot Alignments
        self.top = 100
        self.bottom = 100
        self.left = 75
        self.right = 75
        self.padding = 5

        # Plot Attributes
        self.hover_tool = None
        self.graph_name = None

        # Plot Output Format
        self.output_path = utils.def_path

    # End Default Style---------------------------------------------------------------------------------------------------------------------------------------------------------

    def set_plot_dimensions(self, width=None, height=None):
        if (not (width is None)):
            self.plot_width = width
        if (not (height is None)):
            self.plot_height = height

    def set_plot_colors(self, bg_color=None, plot_color=None):
        if (not (bg_color is None)):
            self.bg_color = bg_color
        if (not (plot_color is None)):
            self.plot_color = plot_color

    def set_text_style(self, font_size=None, font_color=None):
        if (not (font_size is None)):
            self.font_size = font_size
        if (not (font_color is None)):
            self.font_color = font_color

    def set_alignment(self, top=None, bottom=None, left=None, right=None, padding=None):
        if (not (top is None)):
            self.top = top
        if (not (bottom is None)):
            self.bottom = bottom
        if (not (left is None)):
            self.left = left
        if (not (right is None)):
            self.right = right
        if (not (padding is None)):
            self.padding = padding

    def set_hover_tool(self, hover_tool=None):
        if (not (hover_tool is None)):
            self.hover_tool = hover_tool

    def set_name(self, name=None):
        if (not (name is None)):
            self.graph_name = name

    def set_labels_manual(self, plot_label=None, x_axis_label=None, y_axis_label=None, filename=None):
        if (not (plot_label is None)):
            self.plot_title = plot_label
        if (not (filename is None)):
            self.filename = filename
        if (not (x_axis_label is None)):
            self.x_axis_title = x_axis_label
        if (not (y_axis_label is None)):
            self.y_axis_title = y_axis_label


    def set_labels(self, plot_label=None, x_axis_label=None, y_axis_label=None):
        if (plot_label is None):
            plot_label = ""
        if (x_axis_label is None):
            x_axis_label = ""
        if (y_axis_label is None):
            y_axis_label = ""

        # Generate Plot Title
        self.plot_title = go.layout.Title(text=str(plot_label), xref='paper', x=0)

        # Generate Xaxis Title
        self.x_axis_title = go.layout.XAxis(title=go.layout.xaxis.Title(text=str(x_axis_label),
                                                                        font=dict(
                                                                            family='Courier New, monospace',
                                                                            size=int(self.font_size),
                                                                            color=str(self.font_color)
                                                                        )))
        # Generate Yaxis Title
        self.y_axis_title = go.layout.YAxis(title=go.layout.yaxis.Title(text=str(y_axis_label),
                                                                        font=dict(
                                                                            family='Courier New, monospace',
                                                                            size=int(self.font_size),
                                                                            color=str(self.font_color)
                                                                        )))

    # End Standard Style Functions----------------------------------------------------------------------------------------------------------------------------------------------

    def filter_data(self, zero_filter, labels, values):
        if (zero_filter):
            index = 0
            length = len(values)
            while (index < length):
                if (values[index] == 0):
                    values.remove(values[index])
                    if (not (labels is None)):
                        labels.remove(labels[index])
                    length -= 1
                else:
                    index += 1

    # End Utility Function------------------------------------------------------------------------------------------------------------------------------------------------------

    def _render_single_plot(self, module_name, output_format=utils.HTML, output_path=None, auto_open=True):

        # Clean Module Name
        module_name = module_name.split(".")[1]

        # Process Plot
        generated_plot = self.process(output_format)

        # Render Appropriate Format
        if (output_format == utils.PNG):
            out_folder = utils.image_path
            if output_path is not None:
                out_folder = output_path

            utils.make_container(out_folder)

            if self.filename is not None:
                output_name = self.filename + "_" + utils.generate_id() + ".png"
            else:
                output_name = utils.generate_id() + ".png"

            go.Figure(data=generated_plot['data'][0], layout=

            generated_plot["layout"]).write_image(os.path.join(
                out_folder, str(output_name)))

        elif (output_format == utils.PDF):
            out_folder = utils.pdf_path
            if output_path is not None:
                out_folder = output_path

            utils.make_container(out_folder)

            if self.filename is not None:
                output_name = self.filename + "_" + utils.generate_id() + ".pdf"
            else:
                output_name = utils.generate_id() + ".pdf"

            go.Figure(data=generated_plot['data'][0], layout=
            generated_plot["layout"]).write_image(os.path.join(
                out_folder, str(output_name)))

        elif (output_format == utils.HTML):
            out_folder = utils.def_path
            if output_path is not None:
                out_folder = output_path
            utils.make_container(out_folder)

            # Render HTML Plot
            if (utils.detect_notebook()):
                # Initialize Notebook Mode
                init_notebook_mode(connected=True)
                utils.clear_terminal()

                # Render Notebook Plot
                output_name = "notebook_" + str(module_name) + "_" + utils.generate_id() + ".html"
                iplot(go.Figure(data=generated_plot['data'], layout=generated_plot['layout']),
                      filename=os.path.join(
                          out_folder, str(output_name)))
            else:
                # Render Local Plot
                output_name = "loc_" + str(module_name) + "_" + utils.generate_id() + ".html"
                plot(go.Figure(data=generated_plot['data'], layout=
                generated_plot['layout']), filename=os.path.join(out_folder, str(output_name)),
                     auto_open=auto_open)

                return os.path.join(out_folder, str(output_name))
        else:
            # Return Error on Invalid Output Format
            print("ERROR: Invalid Output Format!")
            utils.halt()

    def _render_multi_plot(self, module_name, output_format=utils.HTML, output_path=None, auto_open=True):
        # Clean Module Name
        module_name = module_name.split(".")[1]

        # Render Appropriate Format
        if (output_format == utils.PNG):
            out_folder = utils.image_path
            if output_path is not None:
                out_folder = output_path

            utils.make_container(out_folder)

            output_name = str(module_name) + "_" + utils.generate_id() + ".png"
            go.Figure(data=self.multi_plots).write_image(os.path.join(
                out_folder, str(output_name)))

        elif (output_format == utils.PDF):

            out_folder = utils.pdf_path
            if output_path is not None:
                out_folder = output_path

            utils.make_container(out_folder)

            output_name = str(module_name) + "_" + utils.generate_id() + ".pdf"
            go.Figure(data=self.multi_plots).write_image(os.path.join(
                out_folder, str(output_name)))

        elif (output_format == utils.HTML):

            out_folder = utils.def_path
            if output_path is not None:
                out_folder = output_path

            utils.make_container(out_folder)

            # Render Plot
            if (utils.detect_notebook()):
                # Initialize Notebook Mode
                init_notebook_mode(connected=True)
                utils.clear_terminal()

                # Render Notebook Plot
                output_name = "notebook_" + str(module_name) + "_" + \
                              utils.generate_id() + ".html"
                iplot(go.Figure(data=self.multi_plots, layout=
                self.generate_layout()), filename=os.path.join(
                    out_folder, str(output_name)))

            else:
                # Render Local Plot

                output_name = "loc_" + str(module_name) + "_" + utils.generate_id() + ".html"
                plot(go.Figure(data=self.multi_plots, layout= \
                    self.generate_layout()), filename=os.path.join(
                    out_folder, str(output_name)), auto_open=auto_open)
                return os.path.join(
                    out_folder, str(output_name))
        else:
            # Return Error on Invalid Output Format
            print("ERROR: Invalid Output Format!")
            utils.halt()

    # End Render Functions------------------------------------------------------------------------------------------------------------------------------------------------------
